# A Kockaszámláló Asztaltársaság weboldala

Ez a tároló tartalmazza a http://kockaszamlalo.hu/ weblapjait.

## Összeállítás

A projekt a `hugo` statikus weboldal-generátort használja, azzal lehet előállítani a végleges fájlokat. A célmappát a `-d` kapcsolóval lehet megadni:

    hugo -d result

## Futtatás helyi számítógépen

A projekt jellegéből adódan nem szükséges „rendes” webszerverrel használni, a `hugo`-val is futtatható:

    git clone git@gitlab.com:kockaszamlalo/kockaszamlalo-web.git
    cd kockaszamlalo-web
    hugo server

Ezután elérhető a http://localhost:1313/ címen.

# Blogbejegyzések készítése

A következőket kell megadni a bejegyzések fejlécében:

    ---
    Title: "A bejegyzés címe"
    Date: 2019-08-04T15:43:37+02:00 (publikálás ISO8601 időbélyege)
    Draft: false (amíg csak vázlat, addig true)
    Type: "post" (enélkül nem jelenik meg az RSS-hírcsatornában)
    Summary: "Rövid leírás az RSS-hírcsatorna számára"
    ---
